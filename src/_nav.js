export default {
  items: [
    {
      name: 'Home',
      url: '/home',
      icon: 'icon-speedometer',
    },
    {
      divider: true
    },
    {
      name: 'Ventas',
      url: '/ventas',
      icon: 'icon-puzzle',
      children: [
        {
          name: 'Gestionar Productos',
          url: '/ventas/gestionarProductos',
          icon: 'icon-puzzle'
        },
        {
          name: 'Solicitar Productos',
          url: '/ventas/solicitarProductos',
          icon: 'icon-puzzle'
        },
        {
          name: 'Lista de Comprobantes',
          url: '/ventas/listaComprobantes',
          icon: 'icon-puzzle'
        },
        {
          name: 'Productos Cotizados',
          url: '/ventas/productosCotizados',
          icon: 'icon-puzzle'
        },
        {
          name: 'Clientes Cotizados',
          url: '/ventas/clientesCotizados',
          icon: 'icon-puzzle'
        },
        {
          name: 'Resumenes',
          url: '/ventas/resumenes',
          icon: 'icon-puzzle'
        },
        {
          name: 'Cotizaciones',
          url: '/ventas/cotizaciones',
          icon: 'icon-puzzle'
        },
        {
          name: 'Notas de Credito',
          url: '/ventas/notasCredito',
          icon: 'icon-puzzle'
        },
      ]
    },
    {
      divider: true
    },
    {
      name: 'Compras',
      url: '/compras',
      icon: 'icon-cursor',
    },
    {
      divider: true
    },
    {
      name: 'Reportes',
      url: '/reportes',
      icon: 'icon-pie-chart'
    },
  ]
}
