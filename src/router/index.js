import Vue from 'vue'
import Router from 'vue-router'

// Containers
const DefaultContainer = () => import('@/containers/DefaultContainer')

// Views - Pages
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')

// Vistas propias son diferenciadas con el inicio baz + Nombre
const bazHome = () => import('@/views/propias/home/Home')
const bazGestionarProductos = () => import('@/views/propias/ventas/GestionarProductos')
const bazSolicitarProductos = () => import('@/views/propias/ventas/SolicitarProductos')
const bazListaComprobantes = () => import('@/views/propias/ventas/ListaComprobantes')
const bazProductosCotizados = () => import('@/views/propias/ventas/ProductosCotizados')
const bazClientesCotizados = () => import('@/views/propias/ventas/ClientesCotizados')
const bazResumenes = () => import('@/views/propias/ventas/Resumenes')
const bazCotizaciones = () => import('@/views/propias/ventas/Cotizaciones')
const bazNuevaCotizacion = () => import('@/views/propias/ventas/NuevaCotizacion')
const bazNotasCredito = () => import('@/views/propias/ventas/NotasCredito')
const bazCompras = () => import('@/views/propias/compras/Compras')
const bazReportes = () => import('@/views/propias/reportes/Reportes')

Vue.use(Router)

function configRoutes() {
  return [
    {
      path: '/',
      redirect: '/home',
      // name: 'Home',
      component: DefaultContainer,
      children: [
        {
          path: 'home',
          name: 'Home',
          component: bazHome
        },
        {
          path: 'ventas',
          redirect: '/ventas/cotizaciones',
          name: 'Ventas',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'gestionarProductos',
              name: 'Gestionar Productos',
              component: bazGestionarProductos
            },
            {
              path: 'solicitarProductos',
              name: 'Solicitar Productos',
              component: bazSolicitarProductos
            },
            {
              path: 'listaComprobantes',
              name: 'Lista de Comprobantes',
              component: bazListaComprobantes
            },
            {
              path: 'productosCotizados',
              name: 'Productos Cotizados',
              component: bazProductosCotizados
            },
            {
              path: 'clientesCotizados',
              name: 'Clientes Cotizados',
              component: bazClientesCotizados
            },
            {
              path: 'resumenes',
              name: 'Resumenes',
              component: bazResumenes
            },
            {
              path: 'cotizaciones',
              name: 'Cotizaciones',
              component: bazCotizaciones
            },
            {
              path: 'cotizaciones/nuevaCotizacion',
              name: 'Nueva Cotizacion',
              component: bazNuevaCotizacion
            },
            {
              path: 'notasCredito',
              name: 'Notas de Credito',
              component: bazNotasCredito
            }
          ]
        },
        {
          path: 'compras',
          name: 'Compras',
          component: bazCompras
        },
        {
          path: 'reportes',
          name: 'Reportes',
          component: bazReportes
        }
      ]
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '404',
          name: 'Page404',
          component: Page404
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500
        }
      ]
    }
  ]
}

export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})
